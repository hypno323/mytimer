function timer(){
    var min = 300;
    var timer = setInterval(function(){
        document.querySelector('#timer').innerHTML='00:'+min;
        min--;
        if (min < 0) {
            clearInterval(timer);
        }
    }, 1000);
}

timer();
